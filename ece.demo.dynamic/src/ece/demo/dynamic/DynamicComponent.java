/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.demo.dynamic;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component
public class DynamicComponent {

	@Reference(target="(emf.model.name=order)")
	private ResourceSetFactory rsf;
//	@Reference
//	private EPackage.Registry ePackageRegistry;

	@Activate
	public void activate(BundleContext ctx) {
		URL orderURL = ctx.getBundle().getResource("/data/mega.order");
		if (orderURL == null) {
			System.err.println("Cannot find customer at /data/mega.order");
			return;
		}
		ResourceSet rs = rsf.createResourceSet();
		InputStream orderStream;
		try {
			Resource resource = rs.createResource(URI.createURI("tmp.order"));
			orderStream = orderURL.openStream();
			resource.load(orderStream, null);
			EObject order = resource.getContents().get(0);
			System.out.println("Order is: " + order);
			printAttributes(order);
			order.eClass().getEAllReferences().forEach((ref)->{
				if (ref.isMany()) {
					List<?> result = (List<?>) order.eGet(ref);
					printAttributes(result);
				} else {
					printAttributes((EObject) order.eGet(ref));
				}
			});
		} catch (IOException e) {
			System.err.println("Error loading the mega-order: " + e.getMessage());
		}
	}
	
	private void printAttributes(List<?> eos) {
		for (int i = 0; i < eos.size(); i++) {
			EObject eo = (EObject) eos.get(i);
			EClass eoClass = eo.eClass();
			System.out.println(eoClass.getName() + "." + i);
			eoClass.getEAllAttributes().forEach((sf)->System.out.println(eoClass.getName() + "." + sf.getName() + ": " + eo.eGet(sf)));
		}
	}

	private void printAttributes(EObject eo) {
		EClass eoClass = eo.eClass();
		eoClass.getEAllAttributes().forEach((sf)->System.out.println(eoClass.getName() + "." + sf.getName() + ": " + eo.eGet(sf)));
	}

//	/**
//	 * Creates an order basket file
//	 */
//	@SuppressWarnings("unused")
//	private void createOrderBasket() {
//		ResourceSet resourceSet = rsf.createResourceSet();
//		Resource resource = resourceSet.createResource(URI.createURI("/home/mark/mega.order"));
//		EPackage ePackage = ePackageRegistry.getEPackage("http://gecko.io/order/1.0");
//		EFactory eFactory = ePackageRegistry.getEFactory("http://gecko.io/order/1.0");
//		EClass orderBasketEClass = (EClass) (ePackage.getEClassifier("OrderBasket"));
//		EObject order = eFactory.create(orderBasketEClass);
//		EStructuralFeature orderIdFeature = orderBasketEClass.getEStructuralFeature("orderId");
//		order.eSet(orderIdFeature, "myOrder");
//		resource.getContents().add(order);
//		EStructuralFeature customerFeature = orderBasketEClass.getEStructuralFeature("customer");
//		order.eSet(customerFeature, "Emil Orderer");
//		EStructuralFeature customerIdFeature = orderBasketEClass.getEStructuralFeature("customerId");
//		order.eSet(customerIdFeature, "eo001");
//
//		EClass paymentEClass = (EClass) (ePackage.getEClassifier("Payment"));
//		EObject payment = eFactory.create(paymentEClass);
//		EStructuralFeature paymentIdFeature = paymentEClass.getEStructuralFeature("id");
//		payment.eSet(paymentIdFeature, "paypal01");
//		EStructuralFeature paymentIdentifierFeature = paymentEClass.getEStructuralFeature("identifier");
//		payment.eSet(paymentIdentifierFeature, "emil.orderer@home.de");
//		EEnum paymentType = (EEnum) (ePackage.getEClassifier("PaymentType"));
//		EEnumLiteral paypal = paymentType.getEEnumLiteral("PAYPAL");
//		EStructuralFeature paymentTypeFeature = paymentEClass.getEStructuralFeature("type");
//		payment.eSet(paymentTypeFeature, paypal);
//
//		EStructuralFeature paymentFeature = orderBasketEClass.getEStructuralFeature("payment");
//		order.eSet(paymentFeature, payment);
//
//		EStructuralFeature billingAddressFeature = orderBasketEClass.getEStructuralFeature("billingAddress");
//		order.eSet(billingAddressFeature, createAddress("Dead End Street", "Hometown", "Emil Orderer", ePackage, eFactory));
//		EStructuralFeature deliveryAddressFeature = orderBasketEClass.getEStructuralFeature("deliveryAddress");
//		order.eSet(deliveryAddressFeature, createAddress("Mainstreet", "Ludwigsburg", "Emil Orderer Inc.", ePackage, eFactory));
//
//		List<EObject> orderEntries = new ArrayList<EObject>();
//		orderEntries.add(createOrderEntry("EclipseCon 2017 T-Shirt", "T-Shirt", 2, new String[] {"black", "size M"}, ePackage, eFactory));
//		orderEntries.add(createOrderEntry("EclipseCon 2018 T-Shirt", "T-Shirt", 4, new String[] {"red", "XL", "female"}, ePackage, eFactory));
//		EStructuralFeature entryFeature = orderBasketEClass.getEStructuralFeature("entry");
//		order.eSet(entryFeature, orderEntries);
//		try {
//			resource.save(null);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//	private EObject createAddress(String street, String city, String name, EPackage ePackage, EFactory eFactory) {
//		EClass addressEClass = (EClass) (ePackage.getEClassifier("OrderAddress"));
//		EObject address = eFactory.create(addressEClass);
//		EStructuralFeature nameFeature = addressEClass.getEStructuralFeature("name");
//		address.eSet(nameFeature, name);
//		EStructuralFeature cityFeature = addressEClass.getEStructuralFeature("city");
//		address.eSet(cityFeature, city);
//		EStructuralFeature streetFeature = addressEClass.getEStructuralFeature("street");
//		address.eSet(streetFeature, street);
//		return address;
//	}
//
//	private EObject createOrderEntry(String description, String product, int amount, String[] variant, EPackage ePackage, EFactory eFactory) {
//		EClass entryEClass = (EClass) (ePackage.getEClassifier("OrderEntry"));
//		EObject entry = eFactory.create(entryEClass);
//		EStructuralFeature descriptionFeature = entryEClass.getEStructuralFeature("description");
//		entry.eSet(descriptionFeature, description);
//		EStructuralFeature productFeature = entryEClass.getEStructuralFeature("product");
//		entry.eSet(productFeature, product);
//		EStructuralFeature variantFeature = entryEClass.getEStructuralFeature("variant");
//		entry.eSet(variantFeature, Arrays.asList(variant));
//		EStructuralFeature priceFeature = entryEClass.getEStructuralFeature("amount");
//		entry.eSet(priceFeature, amount);
//		return entry;
//	}

}
