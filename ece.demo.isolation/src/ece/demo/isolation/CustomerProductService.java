/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.demo.isolation;

import customer.Customer;
import product.Catalog;

/**
 * Service interface that provides API for customer and product
 * @author Mark Hoffmann
 * @since 15.10.2018
 */
public interface CustomerProductService {
	
	/**
	 * Returns the customer by its id or <code>null</code>
	 * @param id the customer id
	 * @return the customer or <code>null</code>
	 */
	public Customer getCustomer(String id);
	
	/**
	 * Returns the product catalog or <code>null</code>
	 * @param id the product catalog id
	 * @return the catalog or <code>null</code>
	 */
	public Catalog getProductCatalog(String id);

}
