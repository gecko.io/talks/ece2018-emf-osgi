/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.demo.isolation.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import customer.Address;
import customer.Contact;
import customer.ContactType;
import customer.Customer;
import customer.CustomerFactory;
import ece.demo.isolation.CustomerProductService;
import product.Catalog;
import product.Category;
import product.Product;
import product.ProductFactory;
import product.Variant;
import product.VariantType;

/**
 * Service implementation
 * @author Mark Hoffmann
 * @since 16.10.2018
 */
@Component
public class CustomerProductServiceImpl implements CustomerProductService {

	@Reference
//	@Reference(target="(rsf.name=product)")
	private ResourceSetFactory rsf;
	private BundleContext bundleContext;
	
	@Activate
	public void activate(ComponentContext ctx) {
		bundleContext = ctx.getBundleContext();
	}
	/* 
	 * (non-Javadoc)
	 * @see ece.demo.isolation.CustomerProductService#getCustomer(java.lang.String)
	 */
	@Override
	public Customer getCustomer(String id) {
		System.out.println("ResourceSetFactory: " + rsf);
		String path = String.format("data/%s.customer", id);
		URL url = bundleContext.getBundle().getResource(path);
		if (url == null) {
			throw new IllegalStateException("[customer] Error getting url for '" + id + "'");
		}
		ResourceSet resourceSet = rsf.createResourceSet();
		Resource resource = resourceSet.createResource(URI.createURI("tmp.customer"));
		try {
			InputStream input = url.openStream();
			resource.load(input, null);
			Customer customer = (Customer) resource.getContents().get(0);
			return customer;
		} catch (IOException e1) {
			throw new IllegalStateException("[customer] Error creating input stream for '" + id + "'");
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see ece.demo.isolation.CustomerProductService#getProductCatalog(java.lang.String)
	 */
	@Override
	public Catalog getProductCatalog(String id) {
		String path = String.format("data/%s.product", id);
		URL url = bundleContext.getBundle().getResource(path);
		if (url == null) {
			throw new IllegalStateException("[catalog] Error getting url for '" + id + "'");
		}
		ResourceSet resourceSet = rsf.createResourceSet();
		Resource resource = resourceSet.createResource(URI.createURI("tmp.product"));
		try {
			InputStream input = url.openStream();
			resource.load(input, null);
			Catalog productCatalog = (Catalog) resource.getContents().get(0);
			return productCatalog;
		} catch (IOException e1) {
			throw new IllegalStateException("[catalog] Error creating input stream for '" + id + "'");
		}
	}
	
	@SuppressWarnings("unused")
	private void createCustomer(String id) {
		Customer customer = CustomerFactory.eINSTANCE.createCustomer();
		customer.setId(id);
		customer.setFirstName("Mark");
		customer.setLastName("Hoffmann");
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.YEAR, 1977);
		cal.set(Calendar.DAY_OF_MONTH, 21);
		cal.set(Calendar.MONTH, 4);
		customer.setBirthDate(cal.getTime());
		Address address = CustomerFactory.eINSTANCE.createAddress();
		address.setId("dim01");
		address.setCity("Jena");
		address.setCountry("Germany");
		address.setStreet("Kahlaische Strasse 4");
		address.setZip("07745");
		ResourceSet resourceSet = rsf.createResourceSet();
		Resource addressResource = resourceSet.createResource(URI.createURI("/home/mark/dim01.customer"));
		addressResource.getContents().add(address);
		try {
			addressResource.save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		customer.setAddress(address);
		Contact contact = CustomerFactory.eINSTANCE.createContact();
		contact.setType(ContactType.MAIL);
		contact.setValue("m.hoffmann@data-in-motion.biz");
		customer.getContact().add(contact);
		Resource customerResource = resourceSet.createResource(URI.createURI("/home/mark/cust01.customer"));
		customerResource.getContents().add(customer);
		try {
			customerResource.save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	private void createProductCatalog(String id) {
		Catalog catalog = ProductFactory.eINSTANCE.createCatalog();
		catalog.setId(id);
		catalog.setName("ECE Fan Catalog");
		
		Category categoryShirt = ProductFactory.eINSTANCE.createCategory();
		categoryShirt.setId("shirt01");
		categoryShirt.setLiteral("SHIRT");
		Category categoryECE = ProductFactory.eINSTANCE.createCategory();
		categoryECE.setId("ece01");
		categoryECE.setLiteral("ECE");
		Category categoryECE2017 = ProductFactory.eINSTANCE.createCategory();
		categoryECE2017.setId("ece02");
		categoryECE2017.setLiteral("ECE2017");
		
		catalog.getCategory().add(categoryShirt);
		catalog.getCategory().add(categoryECE);
		catalog.getCategory().add(categoryECE2017);
		
		Product product01 = ProductFactory.eINSTANCE.createProduct();
		product01.setId("ece2018");
		product01.setName("T-Shirt EclipseCon 2018");
		product01.setDescription("Latest release of the EclipseCon shirt from");
		product01.setPrice(12);
		product01.getCategory().add(categoryECE);
		product01.getCategory().add(categoryShirt);
		
		Product product02 = ProductFactory.eINSTANCE.createProduct();
		product02.setId("ece2017");
		product02.setName("T-Shirt EclipseCon 2017");
		product02.setDescription("Historical shirt from the legendary last year EclipseCon");
		product02.setPrice(8);
		product02.getCategory().add(categoryECE2017);
		product02.getCategory().add(categoryShirt);
		
		Variant variantRed = ProductFactory.eINSTANCE.createVariant();
		variantRed.setType(VariantType.COLOR);
		variantRed.setValue("RED");
		product02.getVariants().add(variantRed);
		Variant variantBlue = ProductFactory.eINSTANCE.createVariant();
		variantBlue.setType(VariantType.COLOR);
		variantBlue.setValue("BLUE");
		product01.getVariants().add(variantBlue);
		Variant variantSmall = ProductFactory.eINSTANCE.createVariant();
		variantSmall.setType(VariantType.SIZE);
		variantSmall.setValue("S");
		product01.getVariants().add(variantSmall);
		product02.getVariants().add(variantSmall);
		Variant variantMedium = ProductFactory.eINSTANCE.createVariant();
		variantMedium.setType(VariantType.SIZE);
		variantMedium.setValue("M");
		product01.getVariants().add(variantMedium);
		product02.getVariants().add(variantMedium);
		Variant variantLarge = ProductFactory.eINSTANCE.createVariant();
		variantLarge.setType(VariantType.SIZE);
		variantLarge.setValue("L");
		product02.getVariants().add(variantLarge);
		Variant variantXtraLarge = ProductFactory.eINSTANCE.createVariant();
		variantXtraLarge.setType(VariantType.SIZE);
		variantXtraLarge.setValue("XL");
		product01.getVariants().add(variantXtraLarge);
		
		catalog.getProduct().add(product01);
		catalog.getProduct().add(product02);
		
		ResourceSet resourceSet = rsf.createResourceSet();
		Resource catalogResource = resourceSet.createResource(URI.createURI("/home/mark/catalog01.product"));
		catalogResource.getContents().add(catalog);
		try {
			catalogResource.save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
