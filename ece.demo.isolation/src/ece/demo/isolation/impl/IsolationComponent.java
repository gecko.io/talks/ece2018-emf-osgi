/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.demo.isolation.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import customer.Customer;
import ece.demo.isolation.CustomerProductService;
import product.Catalog;

/**
 * Component to show the product and customer functionality
 * @author Mark Hoffmann
 * @since 16.10.2018
 */
@Component(immediate=true)
public class IsolationComponent {
	
	@Reference
	private CustomerProductService service;
	
	@Activate
	public void activate() {
		loadCustomer("cust01");
		loadProduct("catalog01");
	}

	/**
	 * Loads the customer file
	 */
	private void loadCustomer(String id) {
		try {
			Customer c = service.getCustomer("cust01");
			System.out.println("Customer: " + c);
			System.out.println("Customer address: " + c.getAddress());
			System.out.println("Customer contact: " + c.getContact().get(0));
		} catch (Exception e) {
			System.err.println("[customer] " + e.getMessage());
		}
	}
	
	/**
	 * Loads the customer file
	 */
	private void loadProduct(String id) {
		try {
			Catalog c = service.getProductCatalog("catalog01");
			System.out.println("Catalog: " + c);
			System.out.println("Product 1: " + c.getProduct().get(0));
			System.out.println("Product 2: " + c.getProduct().get(1));
		} catch (Exception e) {
			System.err.println("[product] " + e.getMessage());
		}
	}

}
