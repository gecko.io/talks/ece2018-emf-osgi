/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.demo;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.emf.osgi.EMFNamespaces;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.emf.osgi.annotation.require.RequireEMF;
import org.gecko.emf.osgi.annotation.require.RequireModelByName;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import customer.Customer;

@RequireEMF
@RequireModelByName(name="customer")
@Component(immediate=true)
public class CustomerComponent {

//	@Reference(target="(" + EMFNamespaces.EMF_MODEL_NAME + "=customer)")
//	@Reference
	private ResourceSetFactory rsf;
	
	@Reference(target="(" + EMFNamespaces.EMF_MODEL_NAME + "=customer)")
//	@Reference
	private ResourceSet rs;
	
	@Activate
	public void activate(BundleContext ctx) {
		System.out.println("Activated Customer Component");
		URL custURL = ctx.getBundle().getResource("/data/cust01.customer");
		if (custURL == null) {
			System.err.println("Cannot find customer at /data/cust01.customer");
			return;
		}
		if (rsf != null) {
			rs = rsf.createResourceSet();
		}
		InputStream custStream;
		try {
			Resource custResource = rs.createResource(URI.createURI("tmp.customer"));
			custStream = custURL.openStream();
			custResource.load(custStream, null);
			Customer c = (Customer) custResource.getContents().get(0);
			System.out.println("Customer: " + c);
		} catch (IOException e) {
			System.err.println("Error loading customer");
		} catch (NullPointerException e) {
			System.err.println("Error loading customer, because or no resource set or resource set factory");
		} catch (Exception e) {
			System.err.println("Error loading customer, because: " + e);
			
		}
	}
	
	@Deactivate
	public void deactivate() {
		System.out.println("De-activated Customer Component");
	}

}
